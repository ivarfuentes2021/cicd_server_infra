# README #

## **GoCD with NGINX**


### PREREQUISITES
Make sure you have already installed **Docker Engine** and **Compose tool**.
```bash
$ docker --version
$ docker-compose --version
```

### INSTALLING

```bash
git clone https://ivar_fuentes@bitbucket.org/ivarfuentes2021/cicd_server_infra.git
cd cicd_server_infra
$ docker-compose up -d
```

### TO VERIFY INSTALLATION

go to: http://localhost